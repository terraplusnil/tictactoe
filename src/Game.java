import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class Game extends JFrame {

    Position position = new Position();
    JButton[] buttons = new JButton[Position.SIZE];

    public Game() {
        setLayout(new GridLayout(Position.DIM, Position.DIM));
        setTitle("Game");
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        for (int i = 0; i < Position.SIZE; i++) {
            final JButton button = createButton();
            buttons[i] = button;
            final int idx = i;
            button.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    button.setText(Character.toString(position.turn));
                    position.move(idx);
                    if (!position.isGameEnd()) {
                        int best = position.bestMove();
                        buttons[best].setText(Character.toString(position.turn));
                        position.move(best);
                    }
                    if (position.isGameEnd()) {
                        String message = position.isWinFor('x') ? "Jūs uzvarējāt " : position.isWinFor('o') ? "Dators uzvarēja" : "Neizšķirts";
                        JOptionPane.showMessageDialog(null, message);
                    }
                }
            });
        }
        pack();
        setVisible(true);

    }


    private JButton createButton() {
        JButton button = new JButton();
        button.setPreferredSize(new Dimension(100, 100));
        button.setFont(new Font(null, Font.PLAIN, 50));
        add(button);
        return button;

    }



    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {


            public void run() {
                new Game();

            }
        });
    }
}

